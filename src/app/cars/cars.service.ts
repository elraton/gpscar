import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const CHAT_URL = localStorage.getItem('baseurl').replace('http', 'ws') + '/socket';

@Injectable()
export class CarsService {
    constructor(
        private http: HttpClient,
    ) {}

    addCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/devices', car);
    }

    updateCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.put(base + '/devices/' + car.id, car);
    }

    listCars() {
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/devices');
    }

    deleteCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.delete(base + '/devices/' + car.id);
    }

    addPermission(permission) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/permissions', permission);
    }
}
