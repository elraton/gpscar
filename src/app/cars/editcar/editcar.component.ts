import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { Permission } from 'src/app/models/permission';
import { Car } from 'src/app/models/cars';
import { CarsService } from '../cars.service';
import { ClientService } from 'src/app/clients/client.service';

@Component({
  selector: 'app-edit-car',
  templateUrl: './editcar.component.html',
  styleUrls: ['./editcar.component.less'],
  providers: [PagesService, ClientService]
})
export class EditCarComponent implements OnInit {

  Form: FormGroup;
  clientSelected: Client;
  clients: Client[] = [];
  car: Car = JSON.parse(localStorage.getItem('edit'));
  permission: Permission;

  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  constructor(
    private router: Router,
    private service: CarsService,
    private clientService: ClientService,
  ) { }

  ngOnInit() {
    this.clientService.listUsers().subscribe(
      data => {
        this.clients = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );


    const today = new Date();
    const nextYear = new Date( today.setFullYear(today.getFullYear() + 1) );
    this.Form = new FormGroup({
      name: new FormControl(this.car.name, Validators.required),
      uniqueId: new FormControl(this.car.uniqueId, Validators.required),
      model: new FormControl(this.car.model, Validators.required),
      information: new FormControl(this.car.contact, Validators.required),
      category: new FormControl(this.car.category, Validators.required),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  onSubmit() {
    if (this.Form.valid) {

      this.car.category = this.Form.get('category').value;
      this.car.contact = this.Form.get('information').value;
      this.car.model = this.Form.get('model').value;
      this.car.name = this.Form.get('name').value;
      this.car.uniqueId = this.Form.get('uniqueId').value;

      this.service.updateCar(this.car).subscribe(
        data => {
          this.alerts.push({
            type: 'success',
            message: 'Se guardo el auto',
          });
          setTimeout(() => {
            this.router.navigate(['/cars/list']);
          }, 1000);
        },
        error => {
          this.alerts.push({
            type: 'danger',
            message: 'Ocurrio un error',
          });
        }
      );
    } else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
