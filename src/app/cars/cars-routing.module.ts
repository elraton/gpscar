import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { CarComponent } from './cars.component';
import { AddCarComponent } from './addcar/addcar.component';
import { EditCarComponent } from './editcar/editcar.component';
import { ListCarsComponent } from './listcars/listcars.component';

const routes: Routes = [{
  path: '',
  component: CarComponent,
  children: [
    {
      path: 'list',
      component: ListCarsComponent,
    },
    {
      path: 'add',
      component: AddCarComponent,
    },
    {
      path: 'edit',
      component: EditCarComponent,
    },
    {
      path: 'edit/:id',
      component: EditCarComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarsRoutingModule { }
