import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Car } from 'src/app/models/cars';
import { CarsService } from '../cars.service';
import { LocalDataSource } from 'ng2-smart-table';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-list-cars',
  templateUrl: './listcars.component.html',
  styleUrls: ['./listcars.component.less'],
  providers: [PagesService]
})
export class ListCarsComponent implements OnInit {

  Form: FormGroup;
  cars: Car[] = [];
  cars_parsed = [];
  _success = new Subject<string>();

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="fas fa-plus-circle"></i>',
      createButtonContent: '<i class="fas fa-plus-circle"></i>',
      cancelButtonContent: '<i class="fas fa-plus-circle"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-edit"></i>',
      cancelButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      uniqueId: {
        title: 'GPS numero',
        type: 'string',
      },
      model: {
        title: 'Modelo',
        type: 'string',
      },
      contact: {
        title: 'Información',
        type: 'string',
      },      
      category: {
        title: 'Categoria',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  alerts = [];

  constructor(
    private router: Router,
    private service: CarsService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.service.listCars().subscribe(
      data => {
        this.cars = JSON.parse(JSON.stringify(data));
        this.source.load(this.cars);
      },
      error => {
        console.log(error);
      }
    );
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  onCreate(event): void {
    this.router.navigate(['/cars/add']);
  }

  onEdit(event): void {
    localStorage.setItem('edit', JSON.stringify(event.data));
    this.router.navigate(['/cars/edit']);
  }

  onDelete(event): void {
    if (window.confirm('¿Seguro que quieres borrar?')) {
      this.service.deleteCar(event.data).subscribe(
        success => {
          this.source.remove(event.data);
          const alert = {
            type: 'success',
            message: 'Se elimino correctamente',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          const alert = {
            type: 'danger',
            message: 'Ocurrio un error',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

}
