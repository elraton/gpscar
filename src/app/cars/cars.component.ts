import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { PagesService } from '../pages/pages.service';

@Component({
  selector: 'app-cars',
  styleUrls: ['cars.component.less'],
  templateUrl: 'cars.component.html',
})
export class CarComponent {

  classToggle = '';
  is_admin = (localStorage.getItem('isadmin') === 'true');

  constructor(
    private router: Router,
    private service: PagesService
  ) { }

  toggleMenu() {
    if ( this.classToggle === '' ) {
      this.classToggle = 'on';
    } else {
      this.classToggle = '';
    }
  }

  newUser() {
    this.router.navigate(['/clients/list']);
  }
  newDevice() {
    this.router.navigate(['/cars/list']);
  }
  logout() {
    this.service.logout().subscribe(
      data => {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
      },
      error => {
        console.log(error);
      }
    );
  }
}
