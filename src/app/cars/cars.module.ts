import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarComponent } from './cars.component';
import { AddCarComponent } from './addcar/addcar.component';
import { EditCarComponent } from './editcar/editcar.component';
import { ListCarsComponent } from './listcars/listcars.component';
import { CarsService } from './cars.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CarsRoutingModule } from './cars-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptor } from 'src/app/pages/token.interceptor';
import { PagesService } from 'src/app/pages/pages.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastrModule } from 'ngx-toastr';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

const PAGES_COMPONENTS = [
  CarComponent,
  AddCarComponent,
  EditCarComponent,
  ListCarsComponent,
];

const PROVIDERS = [
  CarsService,
  PagesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

@NgModule({
  declarations: [
    ...PAGES_COMPONENTS
  ],
  imports: [
    CommonModule,
    CarsRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    AngularDateTimePickerModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
    }),
  ],
  providers: [
    ...PROVIDERS
  ]
})
export class CarsModule { }
