import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { Permission } from 'src/app/models/permission';
import { Car } from 'src/app/models/cars';
import { CarsService } from '../cars.service';
import { ClientService } from 'src/app/clients/client.service';

@Component({
  selector: 'app-add-car',
  templateUrl: './addcar.component.html',
  styleUrls: ['./addcar.component.less'],
  providers: [ClientService]
})
export class AddCarComponent implements OnInit {

  Form: FormGroup;
  clientSelected: Client;
  clients: Client[] = [];
  car: Car;
  permission: Permission;

  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  constructor(
    private router: Router,
    private service: CarsService,
    private clientService: ClientService,
  ) { }

  ngOnInit() {
    this.clientService.listUsers().subscribe(
      data => {
        this.clients = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );


    const today = new Date();
    const nextYear = new Date( today.setFullYear(today.getFullYear() + 1) );
    this.Form = new FormGroup({
      name: new FormControl('', Validators.required),
      uniqueId: new FormControl('', Validators.required),
      model: new FormControl('', Validators.required),
      information: new FormControl('', Validators.required),
      category: new FormControl('car', Validators.required),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  onSubmit() {
    if (this.Form.valid) {      

      this.car = {
        id: 0,
        attributes: {},
        category: this.Form.get('category').value,
        contact: this.Form.get('information').value,
        disabled: false,
        geofenceIds: [],
        groupId: 0,
        lastUpdate: null  ,
        model: this.Form.get('model').value,
        name: this.Form.get('name').value,
        phone: '',
        positionId: 0,
        status: null,
        uniqueId: this.Form.get('uniqueId').value,
      };

      this.service.addCar(this.car).subscribe(
        data => {
          this.alerts.push({
            type: 'success',
            message: 'Se agrego el Auto',
          });
          setTimeout(() => {
            this.router.navigate(['/cars/list']);
          }, 1000);
        },
        error => {
          this.alerts.push({
            type: 'danger',
            message: 'Ocurrio un error',
          });
        }
      );
    } else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
