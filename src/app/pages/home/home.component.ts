import { Component, OnInit, HostBinding } from '@angular/core';
import { PagesService } from '../pages.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReportRoutes } from '../../models/report_routes';
import {
  trigger,
  state,
  style,
  query,
  animate,
  transition,
  animateChild,
  // ...
} from '@angular/animations';
import { Device } from 'src/app/models/device';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  animations: [
    trigger('deviceTrigger', [
      state('open', style({flex: '0 0 25%'})),
      state('closed', style({flex: '0 0 45px'})),
      transition('open => closed', [
        animate('0.5s'),
      ]),
      transition('closed => open', [
        animate('0.5s'),
      ]),
    ]),
    trigger('deviceArrow', [
      state('open', style({transform: 'rotate(180deg)'})),
      state('closed', style({transform: 'rotate(0deg)'})),
      transition('open => closed', [animate('100ms')]),
      transition('closed => open', [animate('100ms')]),
    ]),
    trigger('deviceTitle', [
      state('open', style({display: 'none', opacity: 0})),
      state('closed', style({display: 'block', opacity: 1})),
      transition('open => closed', [animate('100ms')]),
      transition('closed => open', [animate('100ms')]),
    ]),
    trigger('deviceCont', [
      state('open', style({display: 'block'})),
      state('closed', style({display: 'none'})),
      transition('open => closed', [animate('1ms')]),
      transition('closed => open', [animate('1ms')]),
    ]),
    trigger('historyDeviceTrigger', [
      state('open', style({height: '70%'})),
      state('closed', style({height: 'calc(100% - 45px)'})),
      transition('open => closed', [animate('0.5s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
    trigger('historyTrigger', [
      state('open', style({height: '30vh'})),
      state('closed', style({height: '45px'})),
      transition('open => closed', [animate('0.5s')]),
      transition('closed => open', [animate('0.5s')]),
    ]),
    trigger('historyArrow', [
      state('open', style({transform: 'rotate(90deg)'})),
      state('closed', style({transform: 'rotate(150deg)'})),
      transition('open => closed', [animate('100ms')]),
      transition('closed => open', [animate('100ms')]),
    ]),
    trigger('historyTitle', [
      state('open', style({display: 'none', opacity: 0})),
      state('closed', style({display: 'block', opacity: 1})),
      transition('open => closed', [animate('100ms')]),
      transition('closed => open', [animate('100ms')]),
    ]),
  ],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.less']
})
export class HomeComponent implements OnInit {

  Form: FormGroup;

  openDevice = true;
  openHistory = false;
  deviceId = 0;
  devices: Device[] = [];

  reportType = 0;
  tableRoutes: ReportRoutes[] = [];

  public positions: Position[];

  classToggle = '';

  is_admin = (localStorage.getItem('isadmin') === 'true');

  constructor(
    private service: PagesService,
    private router: Router,
  ) { }

  ngOnInit() {

  }

  toggleMenu() {
    if ( this.classToggle === '' ) {
      this.classToggle = 'on';
    } else {
      this.classToggle = '';
    }
  }

  newUser() {
    this.router.navigate(['/clients']);
  }
  newDevice() {
    this.router.navigate(['/cars']);
  }
  logout() {
    this.service.logout().subscribe(
      data => {
        localStorage.removeItem('token');
        this.router.navigate(['/']);
      },
      error => {
        console.log(error);
      }
    );
  }

  toggleDevice() {
    this.openDevice = !this.openDevice;
  }

  toggleHistory() {
    this.openHistory = !this.openHistory;
  }

  tableReceived(data: ReportRoutes[]) {
    this.tableRoutes = data;
  }

  reportReceived(data: number) {
    this.reportType = data;
  }

  positionsReceived(data: Position[]) {
    this.positions = data;
  }

  getDeviceId(data: number) {
    this.deviceId = data;
  }

  getDevices(data: Device[]) {
    this.devices = data;
  }

}
