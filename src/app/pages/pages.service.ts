import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, Subject } from 'rxjs/Rx';
import { WebsocketService } from './websocket.service';

const CHAT_URL = localStorage.getItem('baseurl').replace('http', 'ws') + '/socket';

@Injectable()
export class PagesService {
    token = localStorage.getItem('token');
    public messages: Subject<any>;
    constructor(
        private http: HttpClient,
        wsService: WebsocketService
    ) {
        this.messages = <Subject<any>>wsService.connect(CHAT_URL).map(
            (response: MessageEvent): any => {
              const data = JSON.parse(response.data);
              return data;
            }
        );
    }

    getdevices() {
        const base = localStorage.getItem('baseurl');
        const user = JSON.parse(localStorage.getItem('user'));
        if ( localStorage.getItem('isadmin') === 'true' ) {
            return this.http.get(base + '/devices?all=true');
        } else {
            return this.http.get(base + '/devices?all=false&userId=' + user.id);
        }
    }

    getRoutes(devices) {
        // /reports/route
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/reports/route?' + devices);
    }

    getSession(token) {
        // /reports/route
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/session?token=' + token);
    }

    getEvents(devices) {
        // /reports/events
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/reports/events?' + devices);
    }

    getTrips(devices) {
        // /reports/trips
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/reports/trips?' + devices);
    }

    logout() {
        const base = localStorage.getItem('baseurl');
        const form = new HttpParams()
        .set('undefined', 'false');
        const options = {
            // .set('Content-Type', 'application/x-www-form-urlencoded')
            headers: new HttpHeaders().set('Connection', 'keep-alive'),
            withCredentials: true
        };

        return this.http.delete(base + '/session');
    }

    getGroups() {
        const base = localStorage.getItem('baseurl');
        const user = JSON.parse(localStorage.getItem('user'));
        if ( localStorage.getItem('isadmin') === 'true' ) {
            return this.http.get(base + '/groups?all=true');
        } else {
            return this.http.get(base + '/groups?all=false&userId=' + user.id);
        }
    }
}
