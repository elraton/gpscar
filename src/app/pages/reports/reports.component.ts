import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { PagesService } from '../pages.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Device } from '../../models/device';
import { WebsocketService } from "../websocket.service";

import { webSocket } from "rxjs/webSocket";
import { ReportRoutes } from '../../models/report_routes';
import { ReportEvents } from '../../models/report_events';
import { ReportTrips } from '../../models/report_trips';
import { ReportStops } from '../../models/report_stops';
import { Colors } from 'src/app/models/colors';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.less'],
  providers: [WebsocketService, PagesService]
})
export class ReportsComponent implements OnInit {
  @Input() show: boolean;
  @Output() outtable = new EventEmitter<ReportRoutes[]>();
  @Output() outReport = new EventEmitter<number>();
  
  devices: Device[];
  idSelected = 0;

  week_days = ['Dom', 'Lun', 'Mar', 'Mier', ' Jue', 'Vie', 'Sab'];
  months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  report = 0;
  device = 0;
  period = 1;

  subject;

  table_routes: ReportRoutes[] = [];
  table_routes_parsed = [];
  table_events: ReportEvents[] = [];
  table_trips: ReportTrips[] = [];
  table_stops: ReportStops[] = [];

  from = new Date(new Date().setUTCHours(0,0,0)).toISOString();
  to = new Date(new Date().setUTCHours(23,59,59)).toISOString();

  loading = false;

  constructor(private service: PagesService) {
  }

  ngOnInit() {
    this.service.getdevices().subscribe(
      response => {
        this.devices = JSON.parse(JSON.stringify(response));
        this.getRoutes();
        this.getEvents();
        this.getTrips();
        this.getStops();
      },
      error => {
        console.log('error');
        console.log(error);
      }
    );
  }

  deviceChange() {
    this.changeReport();
  }

  changeReport() {
    switch (+this.report) {
      case 1:{
        this.getRoutes();
        break;
      }
      case 2:{
        this.getEvents();
        this.outtable.emit([]);
        break;
      }
      case 3:{
        this.getTrips();
        this.outtable.emit([]);
        break;
      }
      case 4:{
        this.getStops();
        this.outtable.emit([]);
        break;
      }
      default:
        break;
    }
  }

  changePeriod() {
    switch (+this.period) {
      case 1: { // hoy
        this.from = new Date(new Date().setUTCHours(0,0,0)).toISOString();
        this.to = new Date(new Date().setUTCHours(23,59,59)).toISOString();
        this.changeReport();
        break;
      }
      case 2: { // ayer
        const today = new Date();
        this.from = new Date(new Date((today.setDate(today.getDate()-1))).setUTCHours(0,0,0)).toISOString();
        this.to = new Date(new Date((today.setDate(today.getDate()-1))).setUTCHours(23,59,59)).toISOString();
        this.changeReport();
        break;
      }
      case 3: { // esta semana
        const today = new Date();
        const firstDay = new Date( today.setDate(today.getDate() - today.getDay()) );
        this.from = new Date(  firstDay.setUTCHours(0,0,0) ).toISOString();
        this.to = new Date( new Date( today.setDate( firstDay.getDate() + 7 )).setUTCHours(23,59,59) ).toISOString();
        this.changeReport();
        break;
      }
      case 4: { // anterior semana
        const today = new Date();
        const firstDay = new Date( today.setDate(today.getDate() - today.getDay() - 7 ) );
        this.from = new Date(  firstDay.setUTCHours(0,0,0) ).toISOString();
        this.to = new Date( new Date( today.setDate( firstDay.getDate() + 7 )).setUTCHours(23,59,59) ).toISOString();
        this.changeReport();
        break;
      }
      case 5: { // mes actual
        const today = new Date();
        const firstDay = new Date( today.setDate(1) );
        this.from = new Date(  firstDay.setUTCHours(0,0,0) ).toISOString();
        const lastDay = new Date(today.getFullYear(), today.getMonth() + 1, 0);
        this.to = new Date( lastDay.setUTCHours(23,59,59) ).toISOString();
        this.changeReport();
        break;
      }
    
      default:
        break;
    }
  }

  async getRoutes() {
    this.loading = true;
    this.table_routes = [];
    this.table_routes_parsed = [];
    let devices = '';
    let deviceList = JSON.parse(JSON.stringify(this.devices));
    
    for (const xx of deviceList) {
      devices = devices + 'deviceId=' + xx.id + '&';
    }
    
    devices = devices + 'from=' + this.from + '&to=' + this.to;
    await this.service.getRoutes(devices).toPromise().then(data => {
        this.table_routes = JSON.parse(JSON.stringify(data));
      }
    ).catch(
      error => {
        console.log(error);
      }
    );

    let i = -1;
    for (const xx of deviceList) {
      if (this.table_routes.length > 0){
        i++;
        let routes_aux;
        if ( this.device > 0 ) {
          if (xx.id == this.device) {
            routes_aux = this.table_routes.filter( x => x.deviceId == xx.id);
          }
        } else {
          routes_aux = this.table_routes.filter( x => x.deviceId == xx.id);
        }

        if (routes_aux != undefined) {
          if ( routes_aux.length > 0 ) {
            this.table_routes_parsed.push({
              route: 'Inicio',
              color: Colors[i],
              device: this.getDeviceName(routes_aux[0].deviceId),
              valid: routes_aux[0].valid ? 'si' : 'no',
              serverTime: this.formarHour(routes_aux[0].serverTime),
              latitude: routes_aux[0].latitude.toFixed(6),
              longitude: routes_aux[0].longitude.toFixed(6),
              altitude: routes_aux[0].altitude,
              speed: this.getSpeed(routes_aux[0].speed),
            });
            this.table_routes_parsed.push({
              route: 'Fin',
              color: Colors[i],
              device: this.getDeviceName(routes_aux[routes_aux.length - 1].deviceId),
              valid: routes_aux[routes_aux.length - 1].valid ? 'si' : 'no',
              serverTime: this.formarHour(routes_aux[routes_aux.length - 1].serverTime),
              latitude: routes_aux[routes_aux.length - 1].latitude.toFixed(6),
              longitude: routes_aux[routes_aux.length - 1].longitude.toFixed(6),
              altitude: routes_aux[routes_aux.length - 1].altitude,
              speed: this.getSpeed(routes_aux[routes_aux.length - 1].speed),
            });
          }
        }
      }
    }

    this.outtable.emit(this.table_routes);
    this.loading = false;
  }



  getEvents() {
    this.loading = true;
    let devices = '';
    let deviceList = JSON.parse(JSON.stringify(this.devices));
    if ( this.device > 0 ) {
      deviceList = deviceList.filter( x => x.id == this.device );
    }
    for (const xx of deviceList) {
      devices = devices + 'deviceId=' + xx.id + '&';
    }
    devices = devices + 'from=' + this.from + '&to=' + this.to;
    this.service.getEvents(devices).subscribe(
      data => {
        this.table_events = JSON.parse(JSON.stringify(data));
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  getTrips() {
    let devices = '';
    this.loading = true;
    let deviceList = JSON.parse(JSON.stringify(this.devices));
    if ( this.device > 0 ) {
      deviceList = deviceList.filter( x => x.id == this.device );
    }
    for (const xx of deviceList) {
      devices = devices + 'deviceId=' + xx.id + '&';
    }
    devices = devices + 'from=' + this.from + '&to=' + this.to;
    this.service.getTrips(devices).subscribe(
      data => {
        this.table_trips = JSON.parse(JSON.stringify(data));
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }
  getStops() {
    let devices = '';
    this.loading = true;
    let deviceList = JSON.parse(JSON.stringify(this.devices));
    if ( this.device > 0 ) {
      deviceList = deviceList.filter( x => x.id == this.device );
    }
    for (const xx of deviceList) {
      devices = devices + 'deviceId=' + xx.id + '&';
    }
    devices = devices + 'from=' + this.from + '&to=' + this.to;
    this.service.getTrips(devices).subscribe(
      data => {
        this.table_stops = JSON.parse(JSON.stringify(data));
        this.loading = false;
      },
      error => {
        console.log(error);
      }
    );
  }

  formarHour(date) {
    return date.split('T')[0] + '  ' + date.split('T')[1].split('.')[0];
  }

  getSpeed(speed: number) {
    return (speed * 1.852).toFixed(2);
  }

  courseFormatter(value: number) {
    var courseValues = ['Norte', 'NorEste', 'Este', 'SurEste', 'Sur', 'SurOeste', 'Oeste', 'NorOeste'];
    return courseValues[Math.floor(value / 45)];
  }

  formatType(type: string) {
    return type;
  }

  getDeviceName(id: number) {
    return this.devices.find(x => x.id == id).name;
  }

  milli2hours(milliseconds) {
    const hours = milliseconds / (1000 * 60 * 60);
    var minutes = (hours - Math.floor(hours)) * 60;

    return Math.floor(hours) + ' h ' + Math.floor(minutes) + ' m';
  }

  meters2km(meters) {
    return (meters / 1000).toFixed(2);
  }

  selectRoute(data){}
  selectEvent(data){}
  selectTrip(data){}
  selectStops(data){}

}
