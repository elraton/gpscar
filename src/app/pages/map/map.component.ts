import { Component, OnInit, HostBinding, Input, OnChanges, SimpleChanges, AfterViewInit } from '@angular/core';
import { PagesService } from '../pages.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Device } from '../../models/device';
import { Position } from '../../models/positions';
import { Colors } from '../../models/colors';
import { ReportRoutes } from 'src/app/models/report_routes';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.less'],
})
export class MapComponent implements OnChanges, OnInit {

  lat: number = -16.3978613;
  lng: number = -71.5388498;

  latA: number = -16.3978613;
  lngA: number = -71.5388498;

  latB: number = -16.398674;
  lngB: number = -71.537176;
  
  zoom: number = 14;
  colorindicator = 0;

  iconCar = {
    url: './assets/img/car.svg',
    scaledSize: {
      width: 70,
      height: 70
    }
  };

  carsArray = [];

  devOpen = true;
  histoOpen = true;

  polylines: Array<any>;
  maxSpeed: number;

  polyline = [
    {
        latitude:  39.8282,
        longitude: -98.5795,
        speed: 50
    },
     {
        latitude:  38.8282,
        longitude: -108.5795,
        speed: 50
    },
    {
        latitude: 37.772,
        longitude: -122.214,
        speed: 20
    },
    {
        latitude: 21.291,
        longitude: -157.821,
         speed: 20
    },
    {
        latitude: -18.142,
        longitude: 178.431,
        speed: 20
    },
    {
        latitude: -27.467,
        longitude: 153.027,
        speed: 25
    }
  ];

  @Input() positions: Position[] = [];
  firstPosition: Position;
  @Input() deviceId = 0;
  @Input() devices: Device[] = [];

  @Input() historyOpen: boolean;
  @Input() deviceOpen: boolean;

  @Input() tableRoutes: ReportRoutes[] = [];

  currZoom = 14;


  linesList = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges) {

    if ( JSON.parse(JSON.stringify(changes)).devices !== undefined &&
    JSON.parse(JSON.stringify(changes)).devices.currentValue !== undefined) {
      const devices: Device[] = JSON.parse(JSON.stringify(changes)).devices.currentValue;
      for (const xx of devices) {
        const dev = this.devices.find(x => x.uniqueId === xx.uniqueId);
        if (dev) {
          this.devices[this.devices.indexOf(dev)] = xx;
        } else {
          this.devices.push(xx);
        }
        const car = this.carsArray.find(x => x.deviceId === xx.id);
        if (car) {
          car.status = xx.status;
          car.name = xx.name;
          this.carsArray[this.carsArray.indexOf(car)] = car;
        }
      }
    }

    if ( JSON.parse(JSON.stringify(changes)).positions !== undefined &&
    JSON.parse(JSON.stringify(changes)).positions.currentValue !== undefined) {

      const positions: Position[] = JSON.parse(JSON.stringify(changes)).positions.currentValue;

      for (const xx of positions) {
        const car = this.carsArray.find( c => c.deviceId == xx.deviceId);
        if ( car ) {
          car.longitude = xx.longitude;
          car.latitude = xx.latitude;

        } else {
          const device = this.devices.find(x => x.id === xx.deviceId);
          this.carsArray.push({
            deviceId: xx.deviceId,
            longitude: xx.longitude,
            latitude: xx.latitude,
            status: device.status,
            name: device.name,
            speed: xx.speed > 1 ? (xx.speed * 1.85).toFixed(2) : 0,
            totalDistance: (+xx.attributes.totalDistance / 1000).toFixed(2),
            course: xx.course,
            autopan: true
          });
        }

        const line = this.linesList.find ( l => l.deviceId == xx.deviceId);
        if ( line ) {
          line.points.push({
            longitude: xx.longitude,
            latitude: xx.latitude,
          });
        } else {
          let color = Colors[this.colorindicator];
          this.colorindicator = this.colorindicator + 1;
          const point = [];
          point.push({
            longitude: xx.longitude,
            latitude: xx.latitude,
          });
          this.linesList.push({
            deviceId: xx.deviceId,
            color: color,
            points: point
          })
        }
      }

    }

    if (this.tableRoutes.length > 0) {

      for (const xx of this.devices) {
        const tableAux = JSON.parse(JSON.stringify(this.tableRoutes));

        const coordinates = [];
        const devicecoords = tableAux.filter( x => x.deviceId === xx.id);
      }
    }

    if (this.deviceOpen !== this.devOpen || this.historyOpen !== this.histoOpen) {
      this.histoOpen = this.historyOpen;
      this.devOpen = this.deviceOpen;
    }

    if ( this.deviceId > 0 ) {
      this.carsArray.map( c => {
        if ( c.deviceId == this.deviceId ) {
          c.autopan = false;
          this.zoom = 16;
          this.lat = c.latitude;
          this.lng = c.longitude;
        } else {
          c.autopan = true;
        }
      });
    }

  }

  getRandomColor() {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

  ngOnInit() {
    this.histoOpen = this.historyOpen;
    this.devOpen = this.deviceOpen;
  }

}
