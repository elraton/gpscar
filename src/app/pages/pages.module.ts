import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { PagesService } from './pages.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DevicesComponent } from './devices/devices.component';
import { MapComponent } from './map/map.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './/token.interceptor';
import { ReportsComponent } from './reports/reports.component';
import { AgmCoreModule } from '@agm/core';

const PAGES_COMPONENTS = [
  PagesComponent,
  DevicesComponent,
  HomeComponent,
  ReportsComponent,
  MapComponent
];

const PROVIDERS = [
  PagesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

@NgModule({
  declarations: [
    ...PAGES_COMPONENTS
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDq_gSg6xFMMSdjEViRhZxbFjrvfaJITRw'
    })
  ],
  providers: [
    ...PROVIDERS
  ]
})
export class PagesModule { }
