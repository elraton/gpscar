import { Component, OnInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { PagesService } from '../pages.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Device } from '../../models/device';
import { WebsocketService } from "../websocket.service";
import { Position } from 'src/app/models/positions';

import { webSocket } from "rxjs/webSocket";
import { Group } from 'src/app/models/groups';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.less'],
  providers: [WebsocketService, PagesService]
})
export class DevicesComponent implements OnInit {
  @Input() show: boolean;
  @Output() outpositions = new EventEmitter<Position[]>();
  @Output() outDeviceId = new EventEmitter<number>();
  @Output() outDevices = new EventEmitter<Device[]>();

  Form: FormGroup;

  openDevice = true;
  openHistory = true;

  devices: Device[];
  positions: Position[] = [];
  deviceSelected: Position;
  deviceiDSelected: number = 0;

  groups_parsed = [];
  groups_copy: Group[] = [];
  groupselected = { id: 0};
  subgroupselected = { id: 0 };

  idSelected = 0;

  subject;

  week_days = ['Dom', 'Lun', 'Mar', 'Mier', ' Jue', 'Vie', 'Sab'];
  months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  constructor(private service: PagesService) {
  }

  ngOnInit() {

    this.service.getdevices().subscribe(
      response => {
        this.devices = JSON.parse(JSON.stringify(response));
        this.outDevices.emit(this.devices);
        this.initGroups();
      },
      error => {
        console.log('error');
        console.log(error);
      }
    );

    const URL = localStorage.getItem('baseurl').replace('http', 'ws') + '/socket';
    this.subject = webSocket(URL);

    this.subject.subscribe(
      msg => {
        // console.log(JSON.parse(JSON.stringify(msg)));
        const devices: Device[] = JSON.parse(JSON.stringify(msg)).devices;
        if (devices !== undefined) {
          for (const xx of devices) {
            const dev = this.devices.find( x => x.uniqueId === xx.uniqueId);
            if ( dev ) {
              this.devices[this.devices.indexOf(dev)] = xx;
            } else {
              this.devices.push(xx);
            }
          }
          const dev_aux = JSON.parse( JSON.stringify(this.devices) );
          this.devices = [];
          this.devices = dev_aux;
          this.outDevices.emit(this.devices);
        }

        const positions: Position[] = JSON.parse(JSON.stringify(msg)).positions;

        if (positions !== undefined) {
          if (this.positions.length > 0 ) {
            for (const xx of positions) {
              this.positions = this.positions.filter(x => x.deviceId !== xx.deviceId);
              this.positions.push(xx);
              this.deviceSelected = this.positions.find(x => x.deviceId == this.idSelected);
              //console.log(this.deviceSelected);
            }
          } else {
            this.positions = positions;
          }
          this.outpositions.emit(this.positions);
        }

        
      }, // Called whenever there is a message from the server.
      err => console.log(err), // Called if at any point WebSocket API signals some kind of error.
      () => console.log('complete') // Called when connection is closed (for whatever reason).
    );
  }

  initGroups() {
    this.groups_parsed = [];
    this.service.getGroups().subscribe(
      data => {
        this.groups_copy = JSON.parse(JSON.stringify(data));
        if ( localStorage.getItem('isadmin') === 'true' ) {
          for (const xx of this.groups_copy) {
            if (xx.groupId == 0) {
              this.groups_parsed.push({
                active: false,
                attributes: xx.attributes,
                groupId: xx.groupId,
                id: xx.id,
                name: xx.name,
                devices: this.searchDevices(xx.id),
                subgroups: this.searchSubgroups(xx.id)
              })
            }
          }
        } else {
          const user = JSON.parse(localStorage.getItem('user'));
          const mainGroup = this.groups_copy.find( x => x.name == user.name );
          this.groups_parsed.push({
            active: false,
            attributes: mainGroup.attributes,
            groupId: mainGroup.groupId,
            id: mainGroup.id,
            name: mainGroup.name,
            devices: this.searchDevices(mainGroup.id),
            subgroups: this.searchSubgroups(mainGroup.id)
          })
        }

        //console.log(this.groups_parsed);
      },
      error => {
        console.log('error');
        console.log(error);
      }
    );
  }

  selectDevice(id) {    
    if ( this.idSelected == id ) {
      this.idSelected = 0;
      this.outDeviceId.emit(0);
      return;
    }

    this.idSelected = id;
    this.outDeviceId.emit(id);
  }

  searchDevices(id) {
    const devices = [];
    for ( const xx of this.devices ) {
      if ( xx.groupId == id ) {
        devices.push(xx);
      }
    }
    return devices;
  }

  searchSubgroups(id) {
    const groups = JSON.parse(JSON.stringify(this.groups_copy));
    const subgroup = [];
    for ( const xx of groups) {
      if ( xx.groupId == id ) {
        subgroup.push({
          active: false,
          groupId: xx.groupId,
          id: xx.id,
          name: xx.name,
          devices: this.searchDevices(xx.id)
        });
      }
    }
    return subgroup;
  }

  selectGroup(group) {
    if ( this.groupselected == group ) {
      this.groupselected = { id: 0 };
    } else {
      this.groupselected = group;
    }
  }

  selectSubGroup(group) {
    if ( this.subgroupselected == group ) {
      this.subgroupselected = { id: 0 };
    } else {
      this.subgroupselected = group;
    }
  }

  selectRow(device: Device) {
    this.deviceSelected = this.positions.find(x => x.deviceId == device.id);
    this.idSelected = device.id;
    this.outDeviceId.emit(this.idSelected);
  }

  formarHour(date) {
    return date.split('T')[0] + ' ' + date.split('T')[1].split('.')[0];
  }

  getSpeed(speed: number) {
    return (speed * 1.852).toFixed(2);
  }

  courseFormatter(value: number) {
    var courseValues = ['Norte', 'NorEste', 'Este', 'SurEste', 'Sur', 'SurOeste', 'Oeste', 'NorOeste'];
    return courseValues[Math.floor(value / 45)];
  }

  getlastUpdate(lastupdate: Date) {
    const right_now = new Date();
    const last_date = new Date(lastupdate);
    if (last_date.getDate() == right_now.getDate()) {
      const dif_hour = right_now.getHours() - last_date.getHours();
      const dif_mins = last_date.getMinutes() - right_now.getMinutes();
      if (dif_hour == 0) {
        if (dif_mins == 0) {
          return 'en linea';
        } else {
          return dif_mins + ' minutos';
        }
      } else {
        return dif_hour + ' horas ' + dif_mins + ' minutos';
      }
    } else {
      return this.week_days[last_date.getDay()] + ' ' + last_date.getDate() + ' ' + this.months[last_date.getMonth()] + ' ' + last_date.getFullYear();
    }
  }

}
