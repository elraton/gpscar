export interface Device {
    attributes: any,
    category: string,
    contact: string,
    disabled: boolean,
    geofenceIds: any,
    groupId: number,
    id: number,
    lastUpdate: Date,
    model: string,
    name: string,
    phone: string,
    positionId: number,
    status: string,
    uniqueId: string
}