export interface Client {
    id: number;
    name: string;
    email: string;
    readonly: boolean;
    administrator: boolean;
    map: string;
    latitude: number;
    longitude: number;
    zoom: number;
    password: string;
    twelveHourFormat: boolean;
    coordinateFormat: string;
    disabled: boolean;
    expirationTime: Date;
    deviceLimit: number;
    userLimit: number;
    deviceReadonly: boolean;
    limitCommands: boolean;
    poiLayer: string;
    token: string;
    attributes: any;
}
