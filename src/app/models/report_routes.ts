export interface ReportRoutes {
    id:         number;
    attributes: any;
    deviceId:   number;
    type:       null;
    protocol:   string;
    serverTime: string;
    deviceTime: string;
    fixTime:    string;
    outdated:   boolean;
    valid:      boolean;
    latitude:   number;
    longitude:  number;
    altitude:   number;
    speed:      number;
    course:     number;
    address:    null;
    accuracy:   number;
    network:    null;
}
