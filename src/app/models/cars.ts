export interface Car {
    id: number;
    name: string;
    uniqueId: string;
    status: string;
    disabled: boolean;
    lastUpdate: string;
    positionId: number;
    groupId: number;
    phone: string;
    model: string;
    contact: string;
    category: string;
    geofenceIds: number[];
    attributes: any;
}
