export interface ReportTrips {
    deviceId:        number;
    deviceName:      string;
    distance:        number;
    averageSpeed:    number;
    maxSpeed:        number;
    spentFuel:       number;
    startOdometer:   number;
    endOdometer:     number;
    startPositionId: number;
    endPositionId:   number;
    startLat:        number;
    startLon:        number;
    endLat:          number;
    endLon:          number;
    startTime:       string;
    startAddress:    string;
    endTime:         string;
    endAddress:      string;
    duration:        number;
    driverUniqueId:  number;
    driverName:      string;
}
