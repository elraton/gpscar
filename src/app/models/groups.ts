export interface Group {
    id: number;
    name: string;
    groupId: number;
    attributes: any;
}
