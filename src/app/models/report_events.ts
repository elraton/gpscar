export interface ReportEvents {
    id:            number;
    attributes:    any;
    deviceId:      number;
    type:          string;
    serverTime:    string;
    positionId:    number;
    geofenceId:    number;
    maintenanceId: number;
}
