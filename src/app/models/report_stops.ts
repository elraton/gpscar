export interface ReportStops {
    deviceId:      number;
    deviceName:    string;
    distance:      number;
    averageSpeed:  number;
    maxSpeed:      number;
    spentFuel:     number;
    startOdometer: number;
    endOdometer:   number;
    positionId:    number;
    latitude:      number;
    longitude:     number;
    startTime:     string;
    endTime:       string;
    address:       string;
    duration:      number;
    engineHours:   number;
}
