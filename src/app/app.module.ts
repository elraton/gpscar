import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgModule, LOCALE_ID } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthGuard } from './authguard';
import { ToastrModule } from 'ngx-toastr';
import { AgmCoreModule } from '@agm/core';

// registerLocaleData(localeES);

localStorage.setItem('baseurl', 'http://laisladelraton.info:8080/api');

registerLocaleData(localeEs, 'es');


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    AuthGuard,
    { provide: LOCALE_ID, useValue: 'es' },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
