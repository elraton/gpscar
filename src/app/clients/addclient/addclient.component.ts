import { Component, OnInit, HostBinding, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { Group } from 'src/app/models/groups';

@Component({
  selector: 'app-add-client',
  templateUrl: './addclient.component.html',
  styleUrls: ['./addclient.component.less'],
  providers: [PagesService]
})
export class AddClientComponent implements OnInit {

  Form: FormGroup;
  client: Client;
  group: Group;
  @Output() action = new EventEmitter();

  @Input() groupSelected: Group;


  date: Date = new Date();
  users: Client[] = [];
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  next10Year: Date = null;

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  ngOnInit() {
    this.service.listUsers().subscribe(
      data => {
        this.users = JSON.parse(JSON.stringify(data));
      },
      error => {}
    );
    const today = new Date();
    this.next10Year = new Date( today.setFullYear(today.getFullYear() + 10) );
    this.Form = new FormGroup({
      name: new FormControl('', Validators.required),
      user: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      repassword: new FormControl('', Validators.required),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  cancel() {
    this.action.emit('listClients');
  }

  async onSubmit() {
    if (this.Form.valid) {
      

      if ( this.Form.get('password').value == this.Form.get('repassword').value ) {
        this.client = {
          id: 0,
          administrator: false,
          attributes: {},
          coordinateFormat: '',
          deviceLimit: -1,
          deviceReadonly: false,
          disabled: false,
          email: this.Form.get('user').value,
          expirationTime: this.next10Year,
          latitude: 0,
          limitCommands: false,
          longitude: 0,
          map: '',
          name: this.Form.get('name').value,
          password: this.Form.get('password').value,
          poiLayer: '',
          readonly: false,
          token: '',
          twelveHourFormat: false,
          userLimit: 0,
          zoom: 0,
        };

        this.group = {
          attributes: {},
          groupId: this.groupSelected ? this.groupSelected.id : 0,
          id: 0,
          name: this.Form.get('name').value,
        };

        let userid = 0;
        let groupid = 0;

        await this.service.addUser(this.client).toPromise().then(
          data => {
            userid = JSON.parse(JSON.stringify(data)).id;
          }
        );

        await this.service.addGroup(this.group).toPromise().then(
          data => {
            groupid = JSON.parse(JSON.stringify(data)).id;
          }
        );

        let permission = {
          userId: userid,
          groupId: groupid
        }

        await this.service.addPermission(permission).toPromise().then();

        if ( this.groupSelected ) {
          permission = {
            userId: this.users.find(x => x.name == this.groupSelected.name ).id,
            groupId: groupid
          }
          await this.service.addPermission(permission).toPromise().then();
        }

        this.alerts.push({
          type: 'success',
          message: 'Se agrego el Cliente',
        });
        setTimeout(() => {
          this.action.emit('refreshClients');
        }, 1000);
  
      } else {
        this.alerts.push({
          type: 'danger',
          message: 'Las contraseñas deben coincidir',
        });
      }
    } else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
