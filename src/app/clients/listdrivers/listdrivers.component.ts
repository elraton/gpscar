import { Component, OnInit, HostBinding, Output, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { LocalDataSource } from 'ng2-smart-table';
import { EventEmitter } from '@angular/core';
import { Group } from 'src/app/models/groups';

@Component({
  selector: 'app-list-drivers',
  templateUrl: './listdrivers.component.html',
  styleUrls: ['./listdrivers.component.less'],
  providers: [PagesService]
})
export class ListdriversComponent implements OnInit, OnChanges {

  Form: FormGroup;
  users: Client[] = [];
  groups: Group[] = [];
  groups_copy: Group[] = [];
  users_parsed = [];
  @Output() action = new EventEmitter();
  @Input() groupSelected: Group;

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="fas fa-plus-circle"></i>',
      createButtonContent: '<i class="fas fa-plus-circle"></i>',
      cancelButtonContent: '<i class="fas fa-plus-circle"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-edit"></i>',
      cancelButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash"></i>',
      confirmDelete: true,
    },
    actions: {
      edit: false
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  alerts = [];

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  

  ngOnInit() {
    this.service.listUsers().subscribe(
      data => {
        this.users = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
    this.service.getGroups().subscribe(
      data => {
        this.groups_copy = JSON.parse(JSON.stringify(data));
        this.parseGroups();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    this.groupSelected = changes.groupSelected.currentValue;
    this.parseGroups();
  }

  parseGroups() {
    if ( this.groupSelected ) {
      const groups_c = JSON.parse(JSON.stringify(this.groups_copy));
      this.groups = groups_c.filter( x => x.groupId == this.groupSelected.id );
    } else {
      this.groups = JSON.parse(JSON.stringify(this.groups_copy)).filter( x => x.groupId == 0);;
    }
    this.source.load(this.groups);
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  onCreate(event): void {
    this.action.emit('addClient');
    // this.router.navigate(['/cars/add']);
  }

  onEdit(event): void {
    localStorage.setItem('edit', JSON.stringify(event.data));
    this.action.emit('editClient');
  }

  async deleteGroupandUser(data) {
    await this.service.deleteGroup(data).toPromise().then();
    const user = this.users.find( x => x.name == data.name );
    await this.service.deleteUser(user).toPromise().then();
    const alert = {
      type: 'success',
      message: 'Se elimino correctamente',
    };
    this.alerts.push(alert);
    this.action.emit('refreshClients');
    setTimeout(() => {
      this.alerts.splice(this.alerts.indexOf(alert), 1);
    }, 1000);
    
  }

  onDelete(event): void {
    if (window.confirm('¿Seguro que quieres borrar?')) {
      this.deleteGroupandUser(event.data);
      this.source.remove(event.data);
    } else {
      return;
    }
  }

}
