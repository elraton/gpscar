import { Component, OnInit, HostBinding, Output, Input, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { LocalDataSource } from 'ng2-smart-table';
import { EventEmitter } from '@angular/core';
import { Group } from 'src/app/models/groups';
import { Car } from 'src/app/models/cars';

@Component({
  selector: 'app-list-cars',
  templateUrl: './listcars.component.html',
  styleUrls: ['./listcars.component.less'],
  providers: [PagesService]
})
export class ListcarsComponent implements OnInit, OnChanges {

  Form: FormGroup;
  users: Client[] = [];
  cars: Car[] = [];
  cars_copy: Car[] = [];
  users_parsed = [];
  @Output() action = new EventEmitter();
  @Input() groupSelected: Group;

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="fas fa-plus-circle"></i>',
      createButtonContent: '<i class="fas fa-plus-circle"></i>',
      cancelButtonContent: '<i class="fas fa-plus-circle"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-edit"></i>',
      cancelButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      uniqueId: {
        title: 'IMEI num',
        type: 'string',
      },
      model: {
        title: 'IMEI Pass',
        type: 'string',
      }
    },
  };

  source: LocalDataSource = new LocalDataSource();

  alerts = [];

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  ngOnInit() {
    this.service.listCars().subscribe(
      data => {
        this.cars_copy = JSON.parse(JSON.stringify(data));
        this.parseGroups();
      },
      error => {
        console.log(error);
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    this.groupSelected = changes.groupSelected.currentValue;
    console.log(this.groupSelected);
    this.parseGroups();
  }

  parseGroups() {
    if ( this.groupSelected ) {
      const groups_c = JSON.parse(JSON.stringify(this.cars_copy));
      this.cars = groups_c.filter( x => x.groupId == this.groupSelected.id );
    } else {
      this.cars = JSON.parse(JSON.stringify(this.cars_copy)).filter( x => x.groupId == 0);;
    }
    this.source.load(this.cars);
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  onCreate(event): void {
    this.action.emit('addCar');
    // this.router.navigate(['/cars/add']);
  }

  onEdit(event): void {
    localStorage.setItem('edit', JSON.stringify(event.data));
    this.action.emit('editCar');
  }

  onDelete(event): void {
    if (window.confirm('¿Seguro que quieres borrar?')) {
      this.service.deleteCar(event.data).subscribe(
        success => {
          this.source.remove(event.data);
          const alert = {
            type: 'success',
            message: 'Se elimino correctamente',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          const alert = {
            type: 'danger',
            message: 'Ocurrio un error',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

}
