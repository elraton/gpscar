import { Component, OnInit, HostBinding, Output, Input, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { Group } from 'src/app/models/groups';

@Component({
  selector: 'app-edit-client',
  templateUrl: './editclient.component.html',
  styleUrls: ['./editclient.component.less'],
  providers: [PagesService]
})
export class EditClientComponent implements OnInit {

  Form: FormGroup;
  client = JSON.parse(localStorage.getItem('edit'));

  @Output() action = new EventEmitter();

  @Input() groupSelected: Group;

  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  ngOnInit() {
    this.Form = new FormGroup({
      name: new FormControl(this.client.name, Validators.required),
      user: new FormControl(this.client.email, Validators.required),
      password: new FormControl(''),
      repassword: new FormControl(''),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  cancel() {
    
  }

  onSubmit() {
    if (this.Form.valid) {
      this.client.attributes = {'mail.smtp.host': this.Form.get('description').value};
      this.client.email = this.Form.get('user').value;
      this.client.expirationTime = this.Form.get('expiration').value;
      this.client.name = this.Form.get('name').value;
      this.client.password = this.Form.get('password').value === '' ? this.client.password : this.Form.get('password').value;

      this.service.updateUser(this.client).subscribe(
        data => {
          this.alerts.push({
            type: 'success',
            message: 'Se edito el Cliente',
          });
          setTimeout(() => {
            this.router.navigate(['/clients/list']);
          }, 1000);
        },
        error => {
          this.alerts.push({
            type: 'danger',
            message: 'Ocurrio un error',
          });
        }
      );
    }  else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
