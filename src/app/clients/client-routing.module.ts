import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { ClientComponent } from './client.component';
import { AddClientComponent } from './addclient/addclient.component';
import { EditClientComponent } from './editclient/editclient.component';
import { ListClientComponent } from './listclients/listclients.component';

const routes: Routes = [{
  path: '',
  component: ClientComponent,
  children: [
    {
      path: 'list',
      component: ListClientComponent,
    },
    {
      path: 'add',
      component: AddClientComponent,
    },
    {
      path: 'edit',
      component: EditClientComponent,
    },
    {
      path: 'edit/:id',
      component: EditClientComponent,
    },
    {
      path: '',
      redirectTo: 'list',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientRoutingModule { }
