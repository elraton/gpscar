import { Component, OnInit, HostBinding, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { Group } from 'src/app/models/groups';
import { Car } from 'src/app/models/cars';

@Component({
  selector: 'app-add-car',
  templateUrl: './addcar.component.html',
  styleUrls: ['./addcar.component.less'],
  providers: [PagesService]
})
export class AddCarComponent implements OnInit {

  Form: FormGroup;
  client: Car;
  group: Group;
  users = [];
  @Output() action = new EventEmitter();

  @Input() groupSelected: Group;


  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  next10Year: Date = null;

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  ngOnInit() {
    this.service.listUsers().subscribe(
      data => {
        this.users = JSON.parse(JSON.stringify(data));
      },
      error => {}
    );
    this.Form = new FormGroup({
      name: new FormControl('', Validators.required),
      numsim: new FormControl('', Validators.required),
      imeipassword: new FormControl('123456', Validators.required),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  cancel() {
    this.action.emit('listCars');
  }

  async onSubmit() {
    if (this.Form.valid) {

      this.client = {
        id: 0,
        attributes: {},
        category: 'car',
        contact: '',
        disabled: false,
        geofenceIds: [],
        groupId: this.groupSelected.id,
        lastUpdate: null,
        model: this.Form.get('imeipassword').value,
        name: this.Form.get('name').value,
        phone: this.Form.get('numsim').value,
        positionId: 0,
        status: null,
        uniqueId: this.Form.get('numsim').value,
      };

      let deviceId = 0;
      let userId = this.users.find(x => x.name == this.groupSelected.name ).id;

      await this.service.addCar(this.client).toPromise().then(
        data => {
          console.log(data);
          deviceId = JSON.parse(JSON.stringify(data)).id;
        }
      );

      const permission = { userId: userId, deviceId: deviceId }
      await this.service.addPermission(permission).toPromise().then();

      this.alerts.push({
        type: 'success',
        message: 'Se agrego el Cliente',
      });
      setTimeout(() => {
        this.action.emit('listCars');
      }, 1000);

    } else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
