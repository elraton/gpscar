import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

const CHAT_URL = localStorage.getItem('baseurl').replace('http', 'ws') + '/socket';

@Injectable()
export class ClientService {
    constructor(
        private http: HttpClient,
    ) {}

    getGroups() {
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/groups?all=true');
    }

    addUser(user) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/users', user);
    }

    addGroup(group) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/groups', group);
    }

    updateUser(user) {
        const base = localStorage.getItem('baseurl');
        return this.http.put(base + '/users/' + user.id, user);
    }

    listUsers() {
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/users');
    }

    deleteUser(user) {
        const base = localStorage.getItem('baseurl');
        return this.http.delete(base + '/users/' + user.id);
    }
    deleteGroup(group) {
        const base = localStorage.getItem('baseurl');
        return this.http.delete(base + '/groups/' + group.id);
    }

    /** cars */

    addCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/devices', car);
    }

    updateCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.put(base + '/devices/' + car.id, car);
    }

    listCars() {
        const base = localStorage.getItem('baseurl');
        return this.http.get(base + '/devices');
    }

    deleteCar(car) {
        const base = localStorage.getItem('baseurl');
        return this.http.delete(base + '/devices/' + car.id);
    }

    addPermission(permission) {
        const base = localStorage.getItem('baseurl');
        return this.http.post(base + '/permissions', permission);
    }


    /** cars */ 
}
