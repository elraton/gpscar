import { Component, OnInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { Group } from 'src/app/models/groups';
import { ClientService } from '../client.service';
import { LocalDataSource } from 'ng2-smart-table';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-list-clients',
  templateUrl: './listclients.component.html',
  styleUrls: ['./listclients.component.less'],
  providers: [PagesService]
})
export class ListClientComponent implements OnInit {

  Form: FormGroup;
  groups: Group[] = [];
  groups_parsed = [];
  users: Client[] = [];
  users_parsed = [];
  _success = new Subject<string>();
  listClient = true;
  addClient = false;
  editClient = false;

  listCars = true;
  addCars = false;
  editCars = false;

  settings = {
    mode: 'external',
    add: {
      addButtonContent: '<i class="fas fa-plus-circle"></i>',
      createButtonContent: '<i class="fas fa-plus-circle"></i>',
      cancelButtonContent: '<i class="fas fa-plus-circle"></i>',
    },
    edit: {
      editButtonContent: '<i class="fas fa-edit"></i>',
      saveButtonContent: '<i class="fas fa-edit"></i>',
      cancelButtonContent: '<i class="fas fa-edit"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="fas fa-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
      },
      name: {
        title: 'Nombre',
        type: 'string',
      },
      email: {
        title: 'Usuario',
        type: 'string',
      },
      expirationTime: {
        title: 'Fecha de expiración',
        type: 'string',
      },
      attributes: {
        title: 'Descripción',
        type: 'string',
      },
    },
  };

  source: LocalDataSource = new LocalDataSource();

  alerts = [];

  groupSelected: Group = null;

  constructor(
    private router: Router,
    private service: ClientService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.initGroups();
    this.service.listUsers().subscribe(
      data => {
        this.users = JSON.parse(JSON.stringify(data));
      },
      error => {
        console.log(error);
      }
    );
  }

  initGroups() {
    this.groups_parsed = [];
    this.service.getGroups().subscribe(
      data => {
        this.groups = JSON.parse(JSON.stringify(data));
        for (const xx of this.groups) {
          if (xx.groupId == 0) {
            this.groups_parsed.push({
              active: false,
              attributes: xx.attributes,
              groupId: xx.groupId,
              id: xx.id,
              name: xx.name,
              subgroups: this.searchSubgroups(xx.id)
            })
          }
        }
      },
      error => {
        console.log('error');
        console.log(error);
      }
    );
  }

  searchSubgroups(id) {
    const groups = JSON.parse(JSON.stringify(this.groups));
    const subgroup = [];
    for ( const xx of groups) {
      if ( xx.groupId == id ) {
        subgroup.push(xx);
      }
    }
    return subgroup;
  }

  actionReceived(event: string) {
    if ( event == 'addClient' ) {
      this.addClient = true;
      this.listClient = false;
      this.editClient = false;
    }
    if (event == 'editClient') {
      this.addClient = false;
      this.listClient = false;
      this.editClient = true;
    }
    if ( event == 'listClients') {
      this.listClient = true;
      this.addClient = false;
      this.editClient = false;
    }
    if (event == 'refreshClients') {
      this.listClient = true;
      this.addClient = false;
      this.editClient = false;
      this.initGroups();
    }

    if (event == 'addCar') {
      this.addCars = true;
      this.listCars = false;
      this.editCars = false;
    }
    if (event == 'editCar') {
      this.addCars = false;
      this.listCars = false;
      this.editCars = true;
    }
    if (event == 'listCars') {
      this.listCars = true;
      this.addCars = false;
      this.editCars = false;
    }
    if (event == 'refreshCars') {
      this.listCars = true;
      this.addCars = false;
      this.editCars = false;
      this.initGroups();
    }
  }

  backmap() {
    this.router.navigate(['/pages/home']);
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  setActive2(group, item) {
    group.subgroups.map( x => x.active = false);
    item.active = true;
    this.groupSelected = item;
  }

  setActive(item) {
    this.groups_parsed.map( x => {
      x.active = false;
      x.subgroups.map( y => y.active = false);
    });
    item.active = true;
    this.groupSelected = item;
  }

  onCreate(event): void {
    this.router.navigate(['/clients/add']);
  }

  onEdit(event): void {
    const user = this.users.find( x => x.id === event.data.id)
    localStorage.setItem('edit', JSON.stringify(user));
    this.router.navigate(['/clients/edit']);
  }

  async deleteGroupandUser(data) {
    await this.service.deleteGroup(data).toPromise().then();
    const user = this.users.find( x => x.name == data.name );
    const alert = {
      type: 'success',
      message: 'Se elimino correctamente',
    };
    this.alerts.push(alert);
    setTimeout(() => {
      this.alerts.splice(this.alerts.indexOf(alert), 1);
    }, 1000);
    
  }

  onDelete(event): void {
    if (window.confirm('¿Seguro que quieres borrar?')) {
      this.deleteGroupandUser(event);
      this.service.deleteUser(event.data).subscribe(
        success => {
          this.source.remove(event.data);
          const alert = {
            type: 'success',
            message: 'Se elimino correctamente',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.SUCCESS, 'Se elimino correctamente', '');
        },
        error => {
          console.log(error);
          const alert = {
            type: 'danger',
            message: 'Ocurrio un error',
          };
          this.alerts.push(alert);
          setTimeout(() => {
            this.alerts.splice(this.alerts.indexOf(alert), 1);
          }, 1000);
          // this.showToast(NbToastStatus.DANGER, 'Ocurrio un error', '');
        }
      );
    } else {
      return;
    }
  }

}
