import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { ClientComponent } from './client.component';
import { AddClientComponent } from './addclient/addclient.component';
import { EditClientComponent } from './editclient/editclient.component';
import { ListClientComponent } from './listclients/listclients.component';
import { ClientService } from './client.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ClientRoutingModule } from './client-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptor } from 'src/app/pages/token.interceptor';
import { PagesService } from 'src/app/pages/pages.service';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ToastrModule } from 'ngx-toastr';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';
import { ListdriversComponent } from './listdrivers/listdrivers.component';
import { ListcarsComponent } from './listcars/listcars.component';
import { AddCarComponent } from './addcar/addcar.component';
import { EditCarComponent } from './editcar/editcar.component';
import {MatTabsModule} from '@angular/material/tabs';

const PAGES_COMPONENTS = [
  ClientComponent,
  AddClientComponent,
  EditClientComponent,
  ListClientComponent,
  ListdriversComponent,
  ListcarsComponent,
  AddCarComponent,
  EditCarComponent
];

const PROVIDERS = [
  ClientService,
  PagesService,
  {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }
];

@NgModule({
  declarations: [
    ...PAGES_COMPONENTS
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    HttpClientModule,
    FormsModule,
    MatTabsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    AngularDateTimePickerModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right',
      preventDuplicates: false,
    })
  ],
  providers: [
    ...PROVIDERS
  ]
})
export class ClientsModule { }
