import { Component, OnInit, HostBinding, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PagesService } from 'src/app/pages/pages.service';
import { Client } from 'src/app/models/client';
import { ClientService } from '../client.service';
import { Group } from 'src/app/models/groups';
import { Car } from 'src/app/models/cars';

@Component({
  selector: 'app-edit-car',
  templateUrl: './editcar.component.html',
  styleUrls: ['./editcar.component.less'],
  providers: [PagesService]
})
export class EditCarComponent implements OnInit {

  Form: FormGroup;
  client: Car = JSON.parse(localStorage.getItem('edit'));
  group: Group;
  @Output() action = new EventEmitter();

  @Input() groupSelected: Group;


  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: false,
    format: 'dd-MM-yyyy',
    defaultOpen: false
  }

  alerts = [];

  next10Year: Date = null;

  constructor(
    private router: Router,
    private service: ClientService,
  ) { }

  ngOnInit() {
    console.log(this.client);
    this.Form = new FormGroup({
      name: new FormControl(this.client.name, Validators.required),
      numsim: new FormControl(this.client.uniqueId, Validators.required),
      imeipassword: new FormControl(this.client.model, Validators.required),
    });
  }

  close(alert) {
    this.alerts.splice(this.alerts.indexOf(alert), 1);
  }

  cancel() {
    this.action.emit('listCars');
  }

  async onSubmit() {
    if (this.Form.valid) {

      this.client.name = this.Form.get('name').value;
      this.client.uniqueId = this.Form.get('numsim').value;
      this.client.model = this.Form.get('imeipassword').value;

      await this.service.updateCar(this.client).toPromise().then();

      this.alerts.push({
        type: 'success',
        message: 'Se modifico el automovil',
      });
      setTimeout(() => {
        this.action.emit('listCars');
      }, 1000);
    } else {
      this.alerts.push({
        type: 'danger',
        message: 'Debe ingresar todos los campos',
      });
    }
  }

}
