import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class LoginService {
    constructor(
        private http: HttpClient,
    ) {
    }

    login(username: string, password: string) {
        const base = localStorage.getItem('baseurl');
        const form = new HttpParams()
        .set('email', username)
        .set('password', password)
        .set('undefined', 'false');
        let options = {
            // .set('Content-Type', 'application/x-www-form-urlencoded')
            headers: new HttpHeaders().set('Connection', 'keep-alive'),
            withCredentials: true
        };

        return this.http.post(base + '/session', form, options);
    }
}