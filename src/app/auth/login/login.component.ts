import { Component, OnInit } from '@angular/core';
import { LoginService } from '../auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {

  Form: FormGroup;
  loading = false;
  error = false;

  constructor(
    private service: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
    this.generateForm();
  }

  generateForm() {
    this.Form = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(4)]),
      password: new FormControl('', [Validators.required, Validators.minLength(4)]),
    });
  }

  login() {
    this.loading = true;
    this.error = false;
    this.service.login(this.Form.controls.username.value, this.Form.controls.password.value).subscribe(
      res => {
        const data = JSON.parse(JSON.stringify(res));
        localStorage.setItem('token', data.token);
        localStorage.setItem('isadmin', data.administrator);
        localStorage.setItem('user', JSON.stringify(data));
        // console.log(data);
        this.loading = false;
        this.router.navigate(['/pages']);
      },
      error => {
        this.loading = false;
        this.error = true;
        console.log('error');
      }
    );
  }

}